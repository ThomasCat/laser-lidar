/***********************************************
MIT License

Copyright (c) 2016 Owais Shaikh

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURmotorzeroE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
**************************************************/

#include <Servo.h>
int rx = A0;              
int tx = 3;
int calibrate_environment;
int calibrate_protrusion;
int reading[10];
int proximity;
int motorzero = 0; 
Servo motor;

void setup(){
  motor.attach(5);
  Serial.begin(115200);
  pinMode(tx,OUTPUT);
  digitalWrite(tx,LOW);
}

void loop(){
  for(motorzero=0; motorzero<=180; motorzero+=1){
    motor.write(motorzero);
    delay(20);
    proximity = readIR(3);
  	Serial.println(proximity);
  }
  for(motorzero=180; motorzero>=0; motorzero-=1){
    motor.write(motorzero);
    delay(120);
    proximity = readIR(3);
  	Serial.println(proximity);
  }
}

int readIR(int intervals){
  for(int x=0;x<intervals;x++){     
    digitalWrite(tx,LOW);
    delay(1);
    calibrate_environment = analogRead(rx);
    digitalWrite(tx,HIGH);
    delay(1);
    calibrate_protrusion = analogRead(rx);
    reading[x] = calibrate_environment-calibrate_protrusion;
  }
 
  for(int x=0;x<intervals;x++){
    proximity+=reading[x];
  }
  return(proximity/intervals);
}
