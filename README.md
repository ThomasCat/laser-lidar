# Laser  LIDAR

A 3-dimensional laser-based topography mapper I worked on as a hobbyist project.

<a title="$pooky [Public domain], via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Laser-symbol-text.svg"><img width="50" alt="Laser-symbol-text" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Laser-symbol-text.svg/512px-Laser-symbol-text.svg.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://www.osha.gov/SLTC/laserhazards/"><b>Caution</b>: Please handle lasers and electricity with care, use appropriate safety equipment.</a>


### Simple Circuit
<img src="circuit.png" alt="simple circuit" width="285">

Shown above is a sample circuit. There are several design configurations, such as mounting a platter on the servo and keeping the sensor static, mounting the sensor on the servo and sweeping environment, etc. You can also use SimuLink and MATLAB to integrate and visualize readings.

<hr>

[Open-source notices](NOTICE)

<b>License</b>:<br>
<a href="https://opensource.org/licenses/MIT" rel="nofollow"><img src="http://pre13.deviantart.net/4938/th/pre/f/2016/070/3/b/mit_license_logo_by_excaliburzero-d9ur2lg.png" alt="MITL Image" data-canonical-src="https://opensource.org/files/OSIApproved_1.png" width="60"></a><br>[Copyright © Owais Shaikh 2016](LICENSE)
<br><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.